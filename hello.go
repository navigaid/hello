package main

import "fmt"
import "log"
import "net/http"

func main() {
	fmt.Println("hello")
	log.Println("listening on :8000")
	http.ListenAndServe(":8000", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) { fmt.Fprintln(w, "hello") }))
}
