all: hello docker

hello:
	CGO_ENABLED=0 go build -o hello .
	strip hello

docker: hello
	docker build -t registry.gitlab.com/navigaid/hello .
	docker push registry.gitlab.com/navigaid/hello
